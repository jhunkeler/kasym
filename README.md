# What on Earth happened here?

My best friend in graduate school overseas ran into trouble getting these
programs working, so I decided to help her out. What you have here is Fiorella
Castelli's updated release of Robert L. Kurucz's original suite of programs.

Kurucz's website:

http://kurucz.harvard.edu

Castelli's website:

http://wwwuser.oats.inaf.it/castelli

No matter how you look at it, downloading the source code from those sites
file-by-file is extremly tedious and prone to errors. So what exactly did I do?
I ported the Castelli codebase to GNU Autotools in an attempt to make life a
little easier and decided to release it as a tarball.


# Why is this package called "KASyM" and not KURUCZ or CASTELLI?

"KASyM" == "Kurucz ATLAS Synthesis Models"

Castelli took the time to port the archaic VMS code to Linux/Unix, however
I feel like it would be wrong to call it "CASyM" for obvious reasons.

The random documentation on each of the two sites made me realize all of the
programs were tied to each other in one way or another. For example, you can't
run SYNTHE without ATLAS9 or ATLAS12, so there is little point to downloading
these programs individually. That, and I had to call it **something** in order
to distribute it to others.

Keep in mind that I am not rebranding the software itself but rather issuing a
name for the complete software package.


# Why GNU Autotools?

Autotools is simple and convenient.

Compiling each file by hand gets confusing and difficult to track after
a while.


# Compiling

The Intel Fortran compiler (ifort) is *required* to build this suite.

A 30-day trial of `ifort` can be obtained here:

https://software.intel.com/en-us/intel-parallel-studio-xe

This software *will not* compile with `gfortran` or `g77`. Don't even bother.

## STEPS
1. ./configure
2. make

I do not recommend running "make install" the resulting binaries generated
by 'make' rely on files in their parent directories for execution. Unless I get
requests to reorganize the codebase to be more "sane" I'm just going to leave it
alone.


## If `configure` complains about version mismatches

Execute `./autogen.sh` to align your system's version of autotools with the
scripts in your build directory.


# Obtaining additional files

This is where picking and choosing individual datasets comes into play.
If I were to include everything found in this suite the tarball would be ~10GB.

Note:
It is possible to issue a recursive "wget" to mirror the two sites to your
local computer, including all the linelists and extraneous data in one shot, but
that's up to you.

See also:
http://www.gnu.org/software/wget/manual/wget.html


# Disclaimer

I did not write this code. I have no affiliation to Harvard University or INAF.
This software does not appear to be licensed but the BSD license says it best:


```
#!

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```

# Contact Information

Joe Hunkeler, <jhunk@stsci.edu> 

